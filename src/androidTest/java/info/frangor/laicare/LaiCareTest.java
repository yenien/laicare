package info.frangor.laicare;

import android.test.ActivityInstrumentationTestCase2;

/**
 * TODO
 * This is a simple framework for a test of an Application.  See
 * {@link android.test.ApplicationTestCase ApplicationTestCase} for more information on
 * how to write and extend Application tests.
 * <p/>
 * To run this test, you can type:
 * adb shell am instrument -w \
 * -e class info.frangor.laicare.LaiCareTest \
 * info.frangor.laicare.tests/android.test.InstrumentationTestRunner
 */
public class LaiCareTest extends ActivityInstrumentationTestCase2<LaiCare> {

    public LaiCareTest() {
        super("info.frangor.laicare", LaiCare.class);
    }

}
